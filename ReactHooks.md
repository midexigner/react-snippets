# Introducttion

### What is React?
- Hooks are a new feature addition in React verion 16.8 which allow you to use React features without having to write a class
- Ex: State of a component
- Hooks don't work inside classes
#### Reason Set 1
- Understand how <em>**this**</em> keyword works in JavaScript
- Remember to bind event handlers in class components
- Class don't minify very well and make reloading very unreliable
#### Reason Set 2
- There is no particular way to reuse stateful component logic
- HOC and render props patterns do address this problem
- Makes the code harder to follow
- There is need a to share stateful logic in a better way
#### Reason Set 3
- Create components for complex scenarios such as data fetching and subscribing to events
- Ex: Data fetching - In componentDidMount and componentDidUpdate
- Ex: Event listeners - In componentDidMount and componentWillUnmount
- Because of stateful logic - Cannot break components into smaller ones

### Rules of Hooks
- **Only Call Hooks at the Top Level**
- Don't call Hooks inside loops, condition or nested functions
- **Only Call Hooks from React Functions**
- Call them from within React functional components and not any regular JavaScript function

### Summary - useState
- The useState hook lets you add state to functional components
- in classes, the state is always an object.
- with the useState hook, the state doesn't have to be an object.
- The useState hook returns an array with 2 elements.
- The first element is the current value of the state, and the second element is a state setter function.
- New state value depends on the previous state value? You can pass a function to the setter function.
- When dealing with objects or arrays, always make sure to spread your state variable and then call the setter function

### useEffect Hook
- The Effect Hook lets you perform **side effects** in **functional components**
- It is a close replacement for **componentDidMount**, **componentDidUpdate** and **componentWillUnmount**


### Context 
- context provides a way to pass data through the componeny tree without haveing to pass props down manually at every level.

### useReducer
- useReducer is a hook that is used for state mangement
- It is an alternative to useState
- what's the difference?
- useState is build using useReducer
- when to useReducer vs useState?

#### Hooks so far
- useState - state
- useEffect - side effects
- useContext - context API
- useReducer - reducers

#### reducer vs useReducer
| reduce in JavaSctipt   |      useReducer in React      |
|----------|:-------------:|
| array.**reduce**(**reducer**,initialValue) |  useReducer(**reducer**,initialState) | 
| singleValue = reducer(accumulator,itemVaue) |    newState = reducer(currentState, action)   |
| **reducer** method return single value | **useReducer** return a pair of values. [newState,dispatch] |

### useReducer Summary
- useReducer is a hook that is used for state management in React
- useReducer is related to reducer functions
- useReducer(reducer,initialState)
- reducer(currentState,action)
______
###  useReducer with useContext
- useReducer - Local sttate management share state between components - Global state management useReducer + useContext

###  useState with useReducer
| Scenario | useState | useReducer |
|----------|:-------------:|:-------------:|
| Type of state | Number,String,Boolean | Object or Array
| Number of state transitions | one or Two | Too many
| Related state transitions? | No | Yes
| Business logic | No business logic | Complex business logic
| Local vs Global | Local | Global

###  useCallback Hook
    - What?
      - useCallback is a hook that will return a memorized version of the callback function that only changes if one of the dependencies has changed.
    - Why?
      - It is useful when passing callbacks to optimized child components that rely on reference equality to prevent unnecessary renders.

###  useMemo

### Resources

- [Array.prototype.reduce()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce)

- [useReducer (simple state & action)](https://www.youtube.com/watch?v=IL82CzlaCys&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=26&ab_channel=Codevolution)