import React, { useState } from "react";

const HookCounterTwo = () => {
  const initialCount = 0;
  const [count, setCount] = useState(initialCount);

  const incrementFive = ()=>{
    for(let i =0;i<5;i++){
        setCount(prevCount => prevCount + 1)
    }
  }

  return (
    <div>
      <h2>Counter Hook 2 component</h2>
      <p>{count}</p>
      <button onClick={()=>setCount(initialCount)}>Reset Count</button>
      <button onClick={()=>setCount(count - 1)}>Decrement Count</button>
      <button onClick={()=>setCount(count + 1)}>Increment Count</button>
      <button onClick={()=>incrementFive()}>Increment 5 Count</button>
    </div>
  );
};

export default HookCounterTwo;
