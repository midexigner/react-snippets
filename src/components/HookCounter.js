import React,{useState} from 'react'

const HookCounter = () => {
    const [count,setCount]= useState(0)
 

    return (
        <div>
             <h2>Counter Hook component</h2>
            <p>{count}</p>
             <button onClick={()=>setCount(count - 1)}>Decrement Count</button>
             <button onClick={()=>setCount(count + 1)}>Increment Count</button>
        </div>
    )
}

export default HookCounter
