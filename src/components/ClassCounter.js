import React, { Component } from 'react'

export class ClassCounter extends Component {
   constructor(props){
       super(props)
       this.state = {
           count:0
       }
   }
   incrementCount = ()=>{
       this.setState({
        count:this.state.count + 1
       })
   }
   decrementCount = ()=>{
       this.setState({
        count:this.state.count - 1
       })
   }
   componentDidMount(){
       document.title = `Clicked ${this.state.count} times`
   }

   componentDidUpdate(){
       document.title = `Clicked ${this.state.count} times`
   }
    render() {
        return (
            <div>
                <h2>Counter Class component</h2>
                <p>{this.state.count}</p>
                <button onClick={()=>this.decrementCount()}>Decrement Count</button>
                <button onClick={()=>this.incrementCount()}>Increment Count</button>
            </div>
        )
    }
}

export default ClassCounter
