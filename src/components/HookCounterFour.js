import React,{useState} from 'react'

const HookCounterFour = () => {
    const [items,setItems] = useState([]);

    const addItem = () =>{
        setItems([...items,{
            id:items.length,
            value:Math.floor(Math.random() * 10) + 1,
        }])
    }

    return (
        <div>
             <h2> useState with array Hook 4 component</h2>
             <button onClick={addItem}>Add a number</button>
             <ul>
                 {items.map(item=>(
                     <li key={item.id}>{item.value}</li>
                 ))}
             </ul>
        </div>
    )
}

export default HookCounterFour
