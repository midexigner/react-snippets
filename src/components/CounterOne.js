import React,{useReducer} from 'react'

const initialState = 0;
const reducer = (state,action)=>{
        switch(action){
        case 'increment':
            return state + 1
        case 'decrement':
            return state - 1
        case 'reset':
            return initialState
        default:
            return state
    }
}

const CounterOne = () => {
    const [count,dispatch] = useReducer(reducer,initialState)
    return (
        <div>
            <h2> useReducer (simple state & action)</h2>
            <p>COunt: {count}</p>
            <button onClick={()=>{dispatch('increment')}}>Increment</button>
            <button onClick={()=>{dispatch('decrement')}}>Decrement</button>
            <button onClick={()=>{dispatch('reset')}}>Rest</button>
        </div>
    )
}

export default CounterOne
