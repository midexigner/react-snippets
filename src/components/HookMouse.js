import React,{useState,useEffect} from 'react'

const HookMouse = () => {
    const [x,SetX] = useState(0)
    const [y,SetY] = useState(0)

    const  logMousePosition = e=>{
        console.log('Mouse event')
        SetX(e.clientX)
        SetY(e.clientX)
    }

    useEffect(()=>{
        console.log('useEffect called')
        window.addEventListener('mousemove',logMousePosition)
        return () =>{
            console.log('component unmounting code')
            window.removeEventListener('mousemove',logMousePosition)
        }
    },[])
   
    return (
        <div>
            <h2>Mouse Hook Move</h2> 
    <p>X: {x}</p>
    <p>Y: {y}</p>
        </div>
    )
}

export default HookMouse
