import React,{useState} from 'react'

const HookCounterThree = () => {
    const [name,setName]= useState({firstName:'',lastName:''})
    return (
        <div>
              <h2> useState with object Hook 3 component</h2>
            <input type="text" value={name.firstName} onChange={e=> setName({...name,firstName:e.target.value})} />
            <input type="text" value={name.lastName} onChange={e=> setName({...name,lastName:e.target.value})} />
            <p>First name is - {name.firstName}</p>
            <p>Last name is - {name.lastName}</p>
    <code>{JSON.stringify(name)}</code>
        </div>
    )
}

export default HookCounterThree
