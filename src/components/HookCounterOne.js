import React, {useState,useEffect} from 'react'

const HookCounterOne = () => {
    const [count,setCount]= useState(0)

    useEffect(()=>{
        document.title = `Clicked ${count} times`
    })
    
    return (
        <div>
            <h2>Counter Hook 1 Component</h2> 
            <button onClick={()=>setCount(count + 1)}>Count {count} times</button>
        </div>
    )
}

export default HookCounterOne
