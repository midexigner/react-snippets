import React,{useState} from 'react'
import HookMouse from './HookMouse'

const MouseContainer = () => {
    const [display,setDisplay]=useState(true)
    return (
        <div>
            <h2>useEffect with cleanup</h2>
            <button onClick={()=>setDisplay(!display)}>Toggle display</button>
            {display && <HookMouse />}
        </div>
    )
}

export default MouseContainer
