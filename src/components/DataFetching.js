import React,{useState,useEffect} from 'react'
import axios from 'axios'


const DataFetching = () => {
    const [posts,setPosts] = useState([]);
    //const [post,setPost] = useState({});
    //const [id,setId] = useState(1);
    //const [idFormButtonClick,setIdFormButtonClick] = useState(1);

    /*const handleClick=()=>{
        setIdFormButtonClick(id)
    }*/
useEffect(()=>{
    // /${idFormButtonClick}
    axios.get(`https://jsonplaceholder.typicode.com/posts`)
    .then(res =>{
        console.log(res)
        setPosts(res.data)
    })
    .catch(err =>{
        console.log(err)
    })
},[])
    return (
        <div>
            <h2>DataFetching</h2>
           
            {/*<input type='text' value={id} onChange={e=> setId(e.target.value)} />
            <button type="button" onClick={handleClick}> fetch post</button>*/}
            <ul style={{
                maxWidth:'300px',
                margin:'auto',
                backgroundColor:'#3ba4a4',
                display:'block',
                padding:'10px 20px',
                listStyle:'none'
                }}>
            {posts.slice(0,2).map((post)=>(
        <li style={{
            color:'#fff',
            boxShadow:'1px 2px 3px rgba(0,0,0.5)'
        }}>{post.title}</li>
    ))} 
            </ul>
        </div>
    )
}

export default DataFetching
