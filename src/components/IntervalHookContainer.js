import React,{useState,useEffect} from 'react'

const IntervalHookContainer = () => {
    const [count, setCount] = useState(0);

    const tick = ()=>{
        setCount(prevCount =>prevCount + 1)
    }

   
    useEffect(()=>{
        const doSomething = ()=>{
            console.log('somepros');
        }
    const interval = setInterval(tick, 1000)
    return () => {
    clearInterval(interval)
}
    },[doSomething])
    return (
        <div>
             <h2>useEffect with incorrect dependency</h2>
    <p>{count}</p>
        </div>
    )
}

export default IntervalHookContainer
