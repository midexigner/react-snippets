import React, {useState,useEffect} from 'react'

const HookCounterFive = () => {
    const [count,setCount]= useState(0)
    const [name,setName]= useState('')

    useEffect(()=>{
        document.title = `Clicked ${count} times`
    },[count])
    
    return (
        <div>
            <h2>Conditionally run effects Hook 5 Component</h2> 
            <input type="text" value={name} onChange={e=> setName(e.target.value)} />
            <button onClick={()=>setCount(count + 1)}>Count {count} times</button>
        </div>
    )
}

export default HookCounterFive
