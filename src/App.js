import React from 'react';
import './App.css';
import ParentComponent from './components/ParentComponent';



function App() {
  return (
    <div className="app">
  <h2>useCallback Hook</h2>
    <ParentComponent />
    </div>
  );
}

export default App;
